//jshint esversion:6
require("dotenv").config();
const express = require("express");
const bodyParser = require("body-parser");
const ejs = require("ejs");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const port = 3000;
const saltRounds = 10;

const app = express();

app.use(express.static("public"));
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended: true}));

mongoose.connect("mongodb://localhost:27017/user", {useNewUrlParser: true});

const Schema = mongoose.Schema;
const userSchema = new Schema ({
  email: String,
  password: String
});

const User = new mongoose.model("User", userSchema);

app.get("/", function (req, res) {
    res.render("home");
});

app.route("/login")
    .get((req, res) => {
        res.render("login");
    })
    .post((req, res) => {
        const inputEmail = req.body.username;
        const inputPassword = req.body.password;
        User.findOne({email: inputEmail}, (err, user) => {
            if (err) {
                console.log(err);
            } else {
                if (user) {
                    bcrypt.compare(inputPassword, user.password, (err, result) => {
                        if (result) {
                            console.log("User has been found in the database and password matches with database entry");
                            res.render("secrets");
                        } else {
                            console.log("User has been found in the database but password does not match!");
                            res.redirect("/");
                        }
                    });
                } else {
                    console.log("No user with this email has been found in the database");
                    res.redirect("/");
                }
            }
        })
    });

app.route("/register")
    .get((req, res) => {
        res.render("register");
    })
    .post((req, res) => {
        bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
            const user = new User({
                email: req.body.username,
                password: hash
            });
            user.save((err) => {
                if (err) {
                    console.log(err);
                } else {
                    res.render("secrets");
                }
            })
        });
    });

app.listen(port, function () {
    console.log(`Server started on port ${port}`);
});
